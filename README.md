# Twitter Photos to Discord
A simple program that extracts the latest tweets from a specified query and filters for photos. Each tweet's url is then posted into a discord channel via webhook.<br>
A twitter dev account is required to use this tool.

## Web info json
This file needs to be passed when calling the tool<br>
twitter_bearer_token - twitter api bearer token<br>
twitter_url - Endpoint url to query for tweets eg: https://api.twitter.com/2/users/:id/tweets<br>
discord_webhook - Discord webhook url<br>

## Compiling
"GCC" along with "make" is used to compile this tool<br>
Call "make" in this directory to compile the tool<br>
It will generate a binary called "tp2d"<br>

## How to use
Call the "tp2d" binary eg "./tp2d file.json" and it will retrieve 5 of the latest tweets from the user linked in twitter_url. <br>
It will then filter for the latest photos and post to the discord webhook defined by discord_webhook. <br>
If it successfully posted to a channel then the program will return a 0. Otherwise, it will return 255<br>

A "latest_time" file must exist in the same directory that the application is called from.<br>
This is how it determines which tweets are latest.<br>
The "latest_time" file only contains the epoch time.<br>
If the "latest_time" file doesn't exist then it will be generated with the epoch being 0.<br>

For an example on how this can be used, here's a bash script that will call the tool every second for 30 seconds<br>
``` 
#!bin/bash/
POLL_TIME=30
while [$POLL_TIME -gt 0]
do
    ./tp2d info.json
    POLL_TIME=`expr $POLL_TIME - 1`
    sleep 1
done
```

Another example using the program's return value where we poll fast for 10 minutes and then poll slow for 12 hours, exiting if successfully posted
```
#!/bin/bash


# Poll for 10 minutes every 500 milliseconds
#
# Assuming it takes an average of 300 milliseconds to run a program that 
# doesn't post
POLL_TIME=600
RV=255
while [ "$POLL_TIME" -gt 0 ] && [ "$RV" -eq 255 ]
do
	./tp2d info.json; RV=$?
	POLL_TIME=`expr $POLL_TIME - 1`
	if [ "$RV" -eq 255 ]
	then
		sleep 0.2
	fi
done

if [ "$RV" -eq 0 ]
then
	echo "Posted fast!"
	exit 0
fi

# If post wasn't successful
# Poll for 12 hours every 30 seconds
#
# Assuming it takes an average of 300 milliseconds to run a program that 
# didn't post

POLL_TIME=1440

while [ "$POLL_TIME" -gt 0 ] && [ "$RV" -eq 255 ]
do
	./tp2d info.json; RV=$?
	POLL_TIME=`expr $POLL_TIME - 1`
	if [ "$RV" -eq 255 ]
	then
		sleep 29.7
	fi
done

if [ "$RV" -eq 0 ]
then
	echo "Posted slow!"
	exit 0
else
	echo "Couldn't find a post within 12 hours"
	exit 255
fi
```
## Required libraries
libcurl4-openssl-dev<br>

### Links to documentation
https://curl.se/libcurl/<br>
https://github.com/DaveGamble/cJSON<br>
https://developer.twitter.com/en/docs/twitter-api<br>
https://discord.com/developers/docs/intro<br>


