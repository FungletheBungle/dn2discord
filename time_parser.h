#ifndef _TIME_PARSER_
#define _TIME_PARSER_

void (*tssm[8])(char, struct tm *);
void reset_tssm();
uint8_t tssm_state;
uint8_t tssm_error;

#define STATE_YEAR		0
#define STATE_MONTH		1
#define STATE_DAY		2
#define STATE_HOUR		3
#define STATE_MINUTE	4
#define STATE_SECOND	5
#define STATE_MS		6
#define STATE_DONE		7

#endif