#ifndef _TP2D_
#define _TP2D_

#define DEBUG
struct tweet_data
{
	time_t time_data;
	uint8_t is_photo;
	/*
		tweet url
		Defined space as tweet url needs to be generated using the id
		URL size is usually around 53 bytes
	*/
	char turl[256];
	char * media_key;
	/*
		Use expanded_url
	*/
	char * url;
	/*
		Get photo url
	*/
	char * purl;
	char * text;
	char * time_raw;
};

void printf_debug(const char * format, ...);

#endif