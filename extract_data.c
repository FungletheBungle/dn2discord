#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>
#include "tp2discord.h"
#include "cJSON.h"
#include "time_parser.h"
#include "extract_data.h"

int get_text(cJSON * tweet, struct tweet_data * td)
{
	cJSON * text;

	text = cJSON_GetObjectItemCaseSensitive(tweet, "text");
	if (text)
	{
		if (text->valuestring)
		{
			td->text = text->valuestring;
		}
		else
		{
			return -2;
		}
	}
	else
	{
		return -1;
	}

	return 0;
}

int get_media_key(cJSON * tweet, struct tweet_data * td)
{
	cJSON * attachments;
	cJSON * media_keys;
	
	attachments = cJSON_GetObjectItemCaseSensitive(tweet, "attachments");
	media_keys = 0;

	if (attachments)
	{
		media_keys = cJSON_GetObjectItemCaseSensitive(attachments, 
						"media_keys");
	}
	else
	{
		return -1;
	}

	td->media_key = 0;
	if (media_keys)
	{	
		if (media_keys->child)
		{
			if (media_keys->child->valuestring)
			{
				td->media_key = media_keys->child->valuestring;
			}
			else
			{
				return -4;
			}
		}
		else
		{
			return -3;
		}
	}
	else
	{
		return -2;
	}

	return 0;
}

int get_turl(cJSON * tweet, struct tweet_data * td)
{
		cJSON * id;

		id = cJSON_GetObjectItemCaseSensitive(tweet, "id");

		memset(td->turl, 0, sizeof(td->turl));
		if (id)
		{
				/*
				 * Assumed that this is the only account we're reading from
				 * FIX
				 */
				strcat(td->turl, "https://twitter.com/hyxpk/status/");
				strcat(td->turl, id->valuestring);
		}
		else
		{
				return -1;
		}

		return 0;
}

int get_url(cJSON * tweet, struct tweet_data * td)
{
	cJSON * entities;
	cJSON * tweet_urls;
	cJSON * tweet_url;

	entities = cJSON_GetObjectItemCaseSensitive(tweet, "entities");
	tweet_urls = 0;
	tweet_url = 0;

	if (entities)
	{
		tweet_urls = cJSON_GetObjectItemCaseSensitive(entities, "urls");
	}
	else
	{
		return -1;
	}

	if (tweet_urls)
	{
		tweet_url = cJSON_GetObjectItemCaseSensitive(tweet_urls->child, 
					"expanded_url");
	}
	else
	{
		return -2;
	}

	td->url = 0;
	if (tweet_url)
	{
		if (tweet_url->valuestring)
		{
			td->url = tweet_url->valuestring;
		}
		else
		{
			return -4;
		}
	}
	else
	{
		return -3;
	}

	return 0;
}

int get_timedata(cJSON * tweet, struct tweet_data * td)
{
	int i;
	char * tbuffer;
	cJSON * created_at;
	struct tm time_data;

	created_at = cJSON_GetObjectItemCaseSensitive(tweet, "created_at");
	td->time_data = 0;
	if (created_at)
	{
		if (created_at->valuestring)
		{
			td->time_raw = created_at->valuestring;
			memset(&time_data, 0, sizeof(struct tm));
			reset_tssm();
			tbuffer = created_at->valuestring;
			for (i = 0; i < 24; i++)
			{
				tssm[tssm_state](tbuffer[i], &time_data);
			}

			/*
				Check if we're at the end of the state machine
			*/
			if (!tssm_error && tssm_state == STATE_DONE)
			{
				td->time_data = mktime(&time_data);
			}
			else
			{
				return -3;
			}
		}
		else
		{
			return -2;
		}
	}
	else
	{
		return -1;
	}

	return 0;
}

int get_type(cJSON * twitter_data, struct tweet_data *td)
{
	cJSON * media_key;
	cJSON * media_type;
	cJSON * media;
	cJSON * media_info;
	cJSON * includes;

	td->is_photo = 0;
	includes = cJSON_GetObjectItemCaseSensitive(twitter_data, "includes"); 
	media = 0;

	if (includes)
	{
		media = cJSON_GetObjectItemCaseSensitive(includes, "media");
	}
	else
	{
		return -1;
	}

	cJSON_ArrayForEach(media_info, media)
	{
		media_key = cJSON_GetObjectItemCaseSensitive(media_info, "media_key");
		if (media_key && td->media_key)
		{
			if (!strcmp(media_key->valuestring, td->media_key))
			{
				media_type = cJSON_GetObjectItemCaseSensitive(media_info, 
							"type");
				if (media_type)
				{
					if (!strcmp(media_type->valuestring, "photo"))
					{
						td->is_photo = 1;
					}
				}
				else
				{
					printf_debug("Couldn't find media type?!\n");
					td->is_photo = 0;
				}
			}
		}
		else
		{
			printf_debug("Couldn't find media key\n");
			td->is_photo = 0;
		}
	}

	return 0;
}

int get_purl(cJSON * twitter_data, struct tweet_data *td)
{
	cJSON * media_key;
	cJSON * media_url;
	cJSON * media;
	cJSON * media_info;
	cJSON * includes;

	includes = cJSON_GetObjectItemCaseSensitive(twitter_data, "includes"); 
	media = 0;

	if (includes)
	{
		media = cJSON_GetObjectItemCaseSensitive(includes, "media");
	}
	else
	{
		return -1;
	}

	cJSON_ArrayForEach(media_info, media)
	{
		media_key = cJSON_GetObjectItemCaseSensitive(media_info, "media_key");
		if (media_key && td->media_key)
		{
			if (!strcmp(media_key->valuestring, td->media_key))
			{
				media_url = cJSON_GetObjectItemCaseSensitive(media_info, 
							"url");
				if (media_url)
				{
					td->purl = media_url->valuestring;
				}
				else
				{
					printf_debug("Couldn't find photo url?!\n");
				}
			}
		}
		else
		{
			printf_debug("Couldn't find media key\n");
		}
	}

	return 0;
}

